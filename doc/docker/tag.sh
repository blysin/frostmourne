#!/bin/bash

docker tag local/frostmourne:0.7 registry.cn-hangzhou.aliyuncs.com/kechangqing/frostmourne:0.7
docker tag local/frostmourne:0.7 registry.cn-hangzhou.aliyuncs.com/kechangqing/frostmourne:latest
docker tag local/frostmourne:0.7 frostmourne2020/frostmourne:0.7
docker tag local/frostmourne:0.7 frostmourne2020/frostmourne:latest
