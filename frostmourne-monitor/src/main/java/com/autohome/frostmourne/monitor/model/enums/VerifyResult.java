package com.autohome.frostmourne.monitor.model.enums;

public class VerifyResult {

    public static final String TRUE = "TRUE";
    public static final String FALSE = "FALSE";
    public static final String NONE = "NONE";
}
