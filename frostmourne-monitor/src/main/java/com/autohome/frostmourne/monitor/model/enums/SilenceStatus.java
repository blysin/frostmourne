package com.autohome.frostmourne.monitor.model.enums;

public class SilenceStatus {

    public static final String YES = "YES";

    public static final String NO = "NO";
}
