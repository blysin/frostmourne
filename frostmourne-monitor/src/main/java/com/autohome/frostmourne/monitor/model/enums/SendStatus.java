package com.autohome.frostmourne.monitor.model.enums;

public class SendStatus {

    public static final String NONE = "NONE";

    public static final String SUCCESS = "SUCCESS";

    public static final String FAIL = "FAIL";

    public static final String FORBID = "FORBID";
}
