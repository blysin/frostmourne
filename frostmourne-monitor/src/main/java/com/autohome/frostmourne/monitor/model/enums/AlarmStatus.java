package com.autohome.frostmourne.monitor.model.enums;

public class AlarmStatus {

    public final static String OPEN = "OPEN";

    public final static String CLOSE = "CLOSE";
}
