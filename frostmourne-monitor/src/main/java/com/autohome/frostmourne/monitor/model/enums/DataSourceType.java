package com.autohome.frostmourne.monitor.model.enums;

public class DataSourceType {

    public static final String ELASTICSEARCH = "elasticsearch";

    public static final String INFLUXDB = "influxdb";

    public static final String HTTP = "http";
}
